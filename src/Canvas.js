import React, { useMemo, useEffect } from "react";
import { Button, Box } from "theme-ui";
import RenderBox from "./Box";
import { Observer } from "mobx-react";
import { boxes, boxAction } from "./BoxesStore";

const Canvas = ({setSelect, selectedBox}) => {
  // const [stateBoxes, setStateBoxes] = React.useState(boxes);


  return (
    <Observer>
      {() => (
        <Box
          sx={{
            width: "80%",
            backgroundColor: "white",
            height: "100vh",
            position: "relative",
            overflow: "hidden",
          }}
        >
          {Object.values(boxes).map((box) => (
            <RenderBox box={box} selectedBox={selectedBox} setSelect={setSelect}/>
          ))}
        </Box>
      )}
    </Observer>
  );
};

export default Canvas;
