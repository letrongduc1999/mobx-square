
import React from 'react'
import { Button, Flex } from 'theme-ui'

const AllButtons = ({deleteButton, undoButton, createButton ,undoStack, redoButton, undoStackPointer}) => {


  return (
    <Flex p={4} sx={{width: '20%', justifyContent: 'center' ,alignItems: 'center',flexDirection: 'column', backgroundColor: 'white', height: '100vh', borderRight: '1px solid black'}}>
      <Button onClick={createButton} mt={2} sx={{backgroundColor: 'blue', cursor: 'pointer'}}>Create</Button>
      <Button onClick={deleteButton} mt={2} sx={{backgroundColor: 'red', cursor: 'pointer'}}>Delete</Button>
      <Button onClick={undoButton} mt={2} sx={{backgroundColor: 'purple', cursor: 'pointer'}}>Undo</Button>
      <Button onClick={redoButton} mt={2} sx={{backgroundColor: 'green', cursor: 'pointer'}}>Redo</Button>
    </Flex>
  )
}

export default AllButtons 