import { observable } from "mobx";
import { deepObserve } from "mobx-utils";

let allBoxes = {};

const undoInitialState = {
  undoStack: [],
  undoStackPointer: -1,
};

export const undoState = observable({ ...undoInitialState });

export const boxes = observable({ ...allBoxes });

let undoable = true

deepObserve(boxes, (change, path) => {
  if(undoable){
    console.log("x")
    const pointer = ++undoState.undoStackPointer;
    undoState.undoStack.length = pointer;
    undoState.undoStack[pointer] = { change };
    undoable = false
  } 
});


// const undoObserve = () => {
//   deepObserve(boxes, (change, path) => {
//     if(undoable){
//       const pointer = ++undoState.undoStackPointer;
//       undoState.undoStack.length = pointer;
//       undoState.undoStack[pointer] = { change };
//       undoable = false
//     }   
//   });
// };

export const boxAction = (action) => {
  const { width, height, id, color, position } = action;

  switch (action.type) {
    case "ADD_BOX":
      {
        boxes[id] = {
          id,
          width,
          height,
          color,
          position,
        };
        undoable = true
        break;
      }
    case "DELETE":
      {
        delete boxes[id];
        undoable = true
        break;
      }
    case "UNDO":
      {
        if (undoState.undoStackPointer < 0) return;
        undoable = false
        console.log(undoState.undoStack)

        const patch = undoState.undoStack[undoState.undoStackPointer];
        undoState.undoStackPointer--;

        const { change } = patch;
        console.log(change.type)

        switch (change.type) {
          case "update":
            change.object[change.name] = change.oldValue;
            break;
          case "add":
            delete change.object[change.name];
            break;
          case "delete":
            change.object[change.name] = change.oldValue;
            break;
          case "remove":
            change.object[change.name] = change.oldValue;
            break;
        }
        return;

      }
    case "REDO":
      {
        undoable = false
        console.log(undoState.undoStack)

        if (undoState.undoStackPointer === undoState.undoStack.length - 1) return;
        undoState.undoStackPointer++;

        const patch = undoState.undoStack[undoState.undoStackPointer];

        const { change } = patch;
        // console.log(change.object[change.name], change.newValue)
        console.log(change.type)
        switch (change.type) {
          case "update":
            change.object[change.name] = change.newValue;
            break;
          case "add":
            change.object[change.name] = change.newValue;

            break;
          case "delete":
            delete change.object[change.name];

            break;
          case "remove":
            delete change.object[change.name];
            break;
        }
        return;

      }
  }
};
