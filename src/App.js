import logo from "./logo.svg";
import "./App.css";
import React, { useState, useRef, useCallback } from "react";
import { Button, Flex } from "theme-ui";
import AllButtons from "./AllButtons";
import Canvas from "./Canvas";
import { Observer } from "mobx-react";
import { observer } from "mobx-react";
import { boxes, boxAction } from "./BoxesStore";

function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

function App() {
  const [selectedBox, setSelect] = useState(null);

  const dispatch = useCallback((action) => {
    boxAction(action);
  }, []);

  const createButton = () => {
    const width = Math.floor(Math.random() * (300 - 100 + 1) + 100);
    const height = Math.floor(Math.random() * (300 - 100 + 1) + 100);
    dispatch({
      type: "ADD_BOX",
      width: width,
      height: height,
      id: uuidv4(),
      color: `#` + Math.floor(16777215 * Math.random()).toString(16),
      position: {
        x: (window.innerWidth * 0.8) / 2 - width / 2,
        y: window.innerHeight / 2 - height / 2,
      },
    });
  };

  const deleteButton = () => {
    dispatch({
      type: "DELETE",
      id: selectedBox
    });
  };

  // const moveElement = (x, y) => {
  //   dispatch({
  //     type: "MOVE_BOX",
  //     position: {
  //       x: x,
  //       y: y
  //     }
  //   });
  // };

  // const resizeElement = (width, height, x, y) => {
  //   dispatch({
  //     type: "RESIZE_BOX",
  //     width,
  //     height,
  //     position: {
  //       x: x,
  //       y: y
  //     }
  //   });
  // };

  const undoButton = () => {
    dispatch({
      type: "UNDO",
    })
    setSelect(null)
  };

  const redoButton = () => {
    dispatch({
      type: "REDO",
    })
  };

  return (
    <Observer>
      {() => (
        <>
          <Flex>
            <AllButtons
              createButton={createButton}
              deleteButton={deleteButton}
              undoButton={undoButton}
              redoButton={redoButton}
            />
            <Canvas selectedBox={selectedBox} setSelect={setSelect}/>
          </Flex>
        </>
      )}
    </Observer>
  );
}

export default App;
