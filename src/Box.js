import React, { useMemo, useEffect, useRef, useState } from "react";
import { Button, Box } from "theme-ui";
import ResizableRect from "react-resizable-rotatable-draggable";
import { Observer } from "mobx-react";

const RenderBox = ({ box, setSelect, selectedBox }) => {

  const ref = useRef();
  const [width, setWidth] = useState(box.width);
  const [height, setHeight] = useState(box.height);
  const [top, setTop] = useState(box.position.y);
  const [left, setLeft] = useState(box.position.x);

  useEffect(() => {
    setTop(box.position.y);
    setLeft(box.position.x);
    setWidth(box.width);
    setHeight(box.height);
  }, [box]);

  const handleUp = () => {};

  const handleDragEnd = () => {};

  const handleResize = (style, isShiftKey, type) => {
    const { top, left, width, height } = style;
    setWidth(Math.round(width));
    setHeight(Math.round(height));
    setTop(Math.round(top));
    setLeft(Math.round(left));
  };

  const handleDrag = (deltaX, deltaY) => {
    setLeft(left + deltaX);
    setTop(top + deltaY);
  };

  return (
    <Observer>
      {() => (
        <>
          <div
            style={{
              top,
              left,
              width,
              height,
              backgroundColor: box.color,
              position: "absolute",
            }}
            onClick={() => setSelect(box.id)}
          ></div>
          {selectedBox === box.id && (
            <ResizableRect
              ref={ref}
              top={top}
              aspectRatio
              left={left}
              minWidth={10}
              width={width}
              minHeight={10}
              height={height}
              onDrag={handleDrag}
              onResize={handleResize}
              onResizeEnd={handleUp}
              onDragEnd={handleDragEnd}
              zoomable="nw, ne, se, sw"
            />
          )}
        </>
      )}
    </Observer>
  );
};

export default RenderBox;
